import { Component } from '@angular/core';
import { DecimalPipe} from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private age: number;
  private upperLimit: number;
  private lowerLimit: number;

  constructor() {
    this.age = 0;
    this.upperLimit = 0;
    this.lowerLimit = 0;
  }

  private calculate() {
    this.upperLimit = (220 - this.age) * 0.85;
    this.lowerLimit = (220 - this.age) * 0.65;
  }

}
